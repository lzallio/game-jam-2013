
// include game code
include "game/game.inc";

process main()
private
	desktop_width, desktop_height;
begin

	// configuracion segun plataforma
	switch(os_id)
		case OS_IOS:	// iphone
			// resolucion del iphone.
			scale_resolution=04800320;
			scale_resolution_orientation=1;
		end
		case 1003: //android			 
			// detecta resolucion del dispositivo
			scale_resolution = graphic_info(0, 0, G_HEIGHT) * 10000 + graphic_info(0, 0, G_WIDTH) ;
			scale_resolution_aspectratio = SRA_PRESERVE;
		end
		case 1010: // Open Pandora
		    scale_resolution = 04800320;
            full_screen = true;			
		end
		default:
			 // adjust the size of the game to the native resolution
			get_desktop_size(&desktop_width, &desktop_height);
			scale_resolution = desktop_width * 10000 + desktop_height ;
			scale_resolution_aspectratio = SRA_PRESERVE;
			// put the game in full screen
			//scale_mode = SCALE_HQ2X;
			full_screen = true;
		end
	end

	// set resolution and load resources
	game_init();
    
    the_game();

end
