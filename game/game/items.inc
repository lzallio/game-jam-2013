process banana(x,y)
private

	character_process collided=0;
	orig_y;
	float inc_y=1;
end

begin
    ctype = c_scroll;
    file = items_fpg;
    graph=1;
    orig_y = y;
    loop

	    if(collided == 0)
		    if(collided = collision(type character_process))
			    
			    collided.tiredness = 0;
			    break;
		    end	
        end
        
        if (y==orig_y+4)
            inc_y=-1;
        elseif(y==orig_y-4)
            inc_y=1;
        end 
   
        y += inc_y;
        
           
        frame;
    end
    
    while (size>0)
        
        size-=5;

        frame;
    end
end


process hamburger(x,y)
private

	character_process collided=0;
	orig_y;
	float inc_y=1;
end

begin
    ctype = c_scroll;
    file = items_fpg;
    graph=2;
    orig_y = y;
    loop

	    if(collided == 0)
		    if(collided = collision(type character_process))
			    
			    collided.tiredness += 30;
			    break;
		    end	
        end
        
        if (y==orig_y+4)
            inc_y=-1;
        elseif(y==orig_y-4)
            inc_y=1;
        end 
   
        y += inc_y;
        
           
        frame;
    end
    
    while (size>0)
        
        size-=5;

        frame;
    end
end

