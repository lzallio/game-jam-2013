global
	bus_fpg;
end

process bus_process()
public
	char_speed = 4;
end

private
	orig_y;
	float inc_y=1;
	byte anim_running[3] = 1,2,3,4;
	inc_x = 3;
	int bus = 0;
end

begin
    ctype = c_scroll;
	x = round((map.layers[0].size.x*map.tileset.tile_size.x)/8);
	y = (map.layers[0].size.y*map.tileset.tile_size.y)-90;
    file = bus_fpg;
	flags = 0;
	if(rand(0,1) == 1)
		bus = 0;
	else
		bus = 2;
	end
	graph = anim_running[0+bus];
    loop
		if(collision(type character_process))
			alpha = 125;
		else
			alpha = 255;
		end
        if (inc_x != 0)
		    if(graph == anim_running[0+bus])
			    graph = anim_running[1+bus];
		    else
			    graph = anim_running[0+bus];
		    end
		end
        x+=inc_x;
        
        if (x>(map.layers[0].size.x*map.tileset.tile_size.x)-(10 * map.tileset.tile_size.x))
	        if (character.status == "won")
	            if(inc_x>0)
	            inc_x--;
	            end
	        else
	            character.status = "died";
	        end
	    end
                
        frame;
    end
end
