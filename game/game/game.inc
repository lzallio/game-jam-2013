// IMPORTS
import "mod_timers";
import "mod_rand";
import "mod_grproc";
import "mod_wm"
import "mod_key"
import "mod_proc"
import "mod_video"
import "mod_text"
import "mod_map"
import "mod_draw"
import "mod_debug"
import "mod_say";
import "mod_screen";
import "mod_math";
import "mod_time";
import "mod_dir";
import "mod_joy";
import "mod_sound";
import "mod_multi";

include "libraries/tiledmap/tiledmap.lib";
include "libraries/motion_tween.inc";

global
	_tiled_map map; // the map we'll load
	items_fpg;
	latidos;
	music, endmusic;
	ac_timer;
	float sec_float;
	float stage_seconds[5] = 0,0,0,0,0,0;
	string savefilepath;
end

include "game/controls.inc";
include "game/char.inc";

global
	character_process character;
end

include "game/items.inc";
include "game/bus.inc";

global
	bus_process bus;
end

include "game/gui.inc";

function game_init()
begin
	set_fps(30,0);
    set_mode(320,240,16);
	
    // carga recursos
    character_fpg = load_fpg('res/fpg/character.fpg');
    items_fpg = load_fpg('res/fpg/items.fpg');
    bus_fpg = load_fpg('res/fpg/bus.fpg');
    
    // carga sonidos
    latidos = load_wav("res/sounds/latido_5.wav");
    music = load_song("res/sounds/CardiacRunnerLoop1Master.ogg");
    endmusic = load_song("res/sounds/CardiacRunnerEndMaster.ogg");
end

process the_game()
private
    _tiledmap_viewport viewport;
    int i,j;
	current_screen;
	timing;
	record;
	return_to_menu = false;
	byte stages[5]=1,2,3,4,5,6; // lista de pantallas
	byte current_stage;
begin
	loop
        return_to_menu = false; // reset flag status
          
		savefilepath = "score.sav";
		if (fexists(savefilepath))
			load(savefilepath,stage_seconds); // guardamos el avance
		else
			save(savefilepath,stage_seconds); // guardamos un score en blanco
		end
        // pantalla de titulos
	    current_screen = title_screen();
	    while (exists(current_screen)) frame; end
	    current_stage = 0; // restart stage counter to first stage
        
        while( current_stage < sizeof(stages))
		
            return_to_menu = false; // reset flag status
	        // renderiza el mapa cargado
	        tilemap_load_map(map,"res/stages/" + (current_stage+1) + ".tmx");
			timer[0] = 0;
	        tilemap_start(viewport, map, 320, 256);
            
            
            // crea objetos en pantalla
            for (i=0; i< map.objectgroup_count ; i++)
                for (j=0; j< map.objectgroups[i].objectcount ; j++)
                    switch(map.objectgroups[i].objects[j].typename)
                        case "banana":
                            banana(map.objectgroups[i].objects[j].x,map.objectgroups[i].objects[j].y);
                        end                        
						case "hamburger":
                            hamburger(map.objectgroups[i].objects[j].x,map.objectgroups[i].objects[j].y);
                        end
                    end
                end
            end
            
            character = character_process();
            bus = bus_process();
            fat_man_heart();
            
            play_song(music,-1);
            
			touch_pad(); // inicia el touch pad
            // loop del juego
            loop
                viewport.x = character.x - (graphic_info(0, 0, G_WIDTH)/2);
                viewport.y = character.y-160;


                if (exit_status or control(CTRL_BACK))
                    return_to_menu = true;
                    break;
                end
                
                
                if (character.status == "died")
                    write ( 0 , graphic_info(0, 0, G_WIDTH)/2,graphic_info(0, 0, G_HEIGHT) /2,4 , "CARDIAC ARREST! Press ENTER to try again");
                    fade_music_off(1000);
                    while (!control(CTRL_START))
                        frame;
                    end
                    break;
                end
                
                if (character.status == "won")
					sec_float = timer[0];
					sec_float = sec_float /100;
                    write ( 0 , graphic_info(0, 0, G_WIDTH)/2,20,4 , "GREAT, YOU DID IT IN "+sec_float+" SECONDS!");
					ac_timer += sec_float;
					if(stage_seconds[current_stage] > sec_float || stage_seconds[current_stage]==0)
						write ( 0 ,graphic_info(0, 0, G_WIDTH)/2,28,4 , "A NEW RECORD FOR THIS STAGE");
						stage_seconds[current_stage] = sec_float;
						save(savefilepath,stage_seconds);
					end
					write ( 0 , graphic_info(0, 0, G_WIDTH)/2,(28) + 8,4 , "Press ENTER to catch next bus");
					current_stage++;
					
                    stop_song();
                    play_song(endmusic,0);
                    while (!control(CTRL_START))
                        viewport.x = character.x - (graphic_info(0, 0, G_WIDTH)/2);
                        viewport.y = character.y-160;

                        frame;
                    end
                    
                    
                    
                    break;
                end
				sec_float = timer[0];
				sec_float = sec_float /100;
                timing = write(0,graphic_info(0, 0, G_WIDTH) -50,10,0,sec_float);
                record = write(0,graphic_info(0, 0, G_WIDTH) -50,20,0,stage_seconds[current_stage]);
                frame;
				Delete_text(timing);
				Delete_text(record);
            end
			timer[0] = 0;
			sec_float = 0;
            if (is_playing_song())
            stop_song();
            end
            
            Delete_text(ALL_TEXT);
            tiledmap_stop(viewport);
            tilemap_unload_map(map);
            let_me_alone();
            frame;
            
            if (return_to_menu)
                break;
            end
                  
        end
        if (!return_to_menu)
        // espera que se suelte el boton
            while (control(CTRL_START)) frame; end
            // pantalla de felicitaciones al terminar el juego
            current_screen = congratulation_screen();
            while (exists(current_screen)) frame; end
        end
        
        
	end
end