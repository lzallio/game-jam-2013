const
CTRL_LEFT = 0;
CTRL_RIGHT = 1;
CTRL_UP = 2;
CTRL_DOWN = 3;
CTRL_JUMP = 4;
CTRL_RUN = 5;
CTRL_START = 6;
CTRL_BACK = 7;
end

global
keyb_map[7] = _a,_d,_w,_s,_k,_j, _enter, _esc;
joy_map[7] = JOY_HAT_LEFT,JOY_HAT_RIGHT,JOY_HAT_UP,JOY_HAT_DOWN,2,3, 9,8;
end


function control(button)
begin
    if (key(keyb_map[button]))
        return true;
    end
	
	if (button < 4)
		 if (joy_gethat(joy_number() -1 ,0) == joy_map[button])
			return true;
		 end
	else
		if (joy_getbutton(joy_number() -1 ,joy_map[button]))
			return true;
		end
	end
    
    return false;
end 


// control touch
process touch_pad()
private
	int shown = false;
	int map, cursor;
	int updated;
	string indicator_color = "none";
	int waiting;
	type_effect animation;
	current_effect;
	byte _controls[3];
	screen_width, screen_height;
	_control_distance;
begin
	priority = 1;
	
	
	map = map_new(100,100,16);
    drawing_map(0,map);
    drawing_color(rgb(255,255,255));
    draw_fcircle(50,50,45);
    
    cursor = map_new(1,1,16);
    map_clear(0,cursor,rgb(255,255,255));
    
    z = -50;
    flags = 4;
	loop
		screen_width = map_info(0,0,G_WIDTH);
		screen_height = map_info(0,0,G_HEIGHT);
	
		_controls[0] = 0;
		_controls[1] = 0;
		_controls[2] = 0;
		_controls[3] = 0;
		
		// mostrar control
		if ( !shown and multi_info(0, "ACTIVE") )
			shown = true;
			graph = map;
			
			x = multi_info(0, "X");
			y = multi_info(0, "Y");
			
			// ajusta la posicion del control en 4 posibles predefinidas			
			if (multi_info(0, "X") < screen_width/2 and multi_info(0, "Y") < screen_height/2)
				x = screen_width/4;
				y = screen_height/4;
				
				y += 10;
				x -= 10;
			end
			
			if (multi_info(0, "X") < screen_width/2 and multi_info(0, "Y") > screen_height/2)
				x = screen_width/4;
				y = screen_height/4 * 3;
				
				y -= 10;
				x -= 10;
			end
			
			if (multi_info(0, "X") > screen_width/2 and multi_info(0, "Y") < screen_height/2)
				x = screen_width/4 * 3;
				y = screen_height/4 ;
				
				y += 10;				
				x += 10;
			end
			
			if (multi_info(0, "X") > screen_width/2 and multi_info(0, "Y") > screen_height/2)
				x = screen_width/4 * 3;
				y = screen_height/4 * 3;
				
				y -= 10;
				x += 10;
			end
			
			mouse.graph = cursor;
				
			
			if (exists(current_effect))
				signal(current_effect,s_kill);
			end
			
			if (size == 100)
				size = 0;
			end
			
			// asigna animacion para mostrarlo
			animation.start = 0;
			animation.duration = 15;
			animation.toValue = 100;
			animation.fromValue = size;
			animation.effectType = motion_effect.elasticEaseOut;
			animation.property = &size;
			
			fixInitialValue(&animation, 1);
			current_effect = applyEffect(&animation);
		end
		
		// esconder control
		if (shown and !multi_info(0, "ACTIVE") and waiting==0)
			waiting = 15; //1 segundo
			if (exists(current_effect))
				signal(current_effect,s_kill);
			end
			// asigna animacion para mostrarlo
			animation.start = 0;
			animation.duration = waiting;
			animation.toValue = 0;
			animation.fromValue = size;
			animation.effectType = motion_effect.strongEaseIn;
			animation.property = &size;
			
			fixInitialValue(&animation, 1);
			current_effect = applyEffect(&animation);				
		end
		// espera a esconder el boton
		if (waiting > 0 and shown and !multi_info(0, "ACTIVE"))
			waiting--;
			if (waiting == 0)
				graph = 0;
				shown = false;
				mouse.graph = 0;				
			end 
		end
		// si se vuelve a tocar la pantalla entonces se resetea el contador
		if (waiting > 0 and shown and multi_info(0, "ACTIVE"))
			waiting = 0;
			if (exists(current_effect))
				signal(current_effect,s_kill);
			end
			// asigna animacion para mostrarlo
			animation.start = 0;
			animation.duration = 15;
			animation.toValue = 100;
			animation.fromValue = size;
			animation.effectType = motion_effect.elasticEaseOut;
			animation.property = &size;
			
			fixInitialValue(&animation, 1);
			current_effect = applyEffect(&animation);
		end
		
		// si el control se esta escondiendo y se aprieta la pantalla en otro lado
		// TODO: esconder el control del todo y hacerlo aparecer.
		
		
		// si el dedo se salio del pad
		if (shown and !collision_finger(0) and indicator_color != "red")
			drawing_map(0,map);
			drawing_color(rgb(255,0,0));
			draw_circle ( 50 , 50 , 46 );
			indicator_color = "red";
		elseif(shown and indicator_color != "green" and collision_finger(0))
			drawing_map(0,map);
			drawing_color(rgb(0,255,0));
			draw_circle ( 50 , 50 , 46 );
			indicator_color = "green"; 
		end
		// si se muestra el pad, y esta dentro del mismo mueve
		if (shown and fget_dist( x, y , multi_info(0, "X") , multi_info(0, "Y") )>10 and collision_finger(0) and multi_info(0, "ACTIVE"))
			_control_distance = fget_dist( x, y , multi_info(0, "X") , multi_info(0, "Y") ) - 10;
			switch(fget_angle ( x, y , multi_info(0, "X") , multi_info(0, "Y") )/1000)
				case 45..135:
					//top
					_controls[0] = 1;
				end
				case 0..45:
					//rigth
					_controls[1] = 1;
				end
				case 315..360:
					//rigth
					_controls[1] = 1;
				end
				case -45..0:
					//rigth
					_controls[1] = 1;
				end
				case 225..315:
					//bottom
					_controls[2] = 1;
				end
				case -90..-45:
					//bottom
					_controls[2] = 1;
				end
				case 135..225:
					//left
					_controls[3] = 1;
				end				
			end	
		end
		
		frame;
	end
onexit
	unload_map(0,map);
	unload_map(0,cursor);
	
end

process collision_finger(finger_number)

begin
	graph = map_new(10, 10, 16);
	
	   
    drawing_map(0, graph);
	map_clear(0,graph,rgb(0,0,0));
    drawing_color(rgb(0, 255, 255));
	
	x = multi_info(finger_number, "X");
	y = multi_info(finger_number, "Y");

	if(multi_info(finger_number, "ACTIVE") > 0.0)
		draw_fcircle(5,5,5);
	end;
	
	return collision(father);
	
	onexit:
    unload_map(0, graph);
end