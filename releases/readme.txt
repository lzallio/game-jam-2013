Cardiac Runner for Windows
========================================

Thank you for playing Cardiac Runner!

 - Facebook Page: https://www.facebook.com/MonkeyVsRobots
 - iOS Download : http://bit.ly/V0imjG
 - Homepage: http://www.pixelatom.com/games/monkey-robots/


Controls
--------


### Keyboard:



 * Esc: Exit.
 * a: Move left.
 * b: Move right.
 * j: Run.
 * k: Jump.

