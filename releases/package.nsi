;Icon "MvR.ico"
OutFile "CardiacRunner.exe"
SilentInstall silent

Section

 InitPluginsDir
 SetOutPath "$PLUGINSDIR"

 ;adding files
 File /r "bin\*.*"

 ;executing app
 ExecWait "$PLUGINSDIR\main.exe"

SectionEnd