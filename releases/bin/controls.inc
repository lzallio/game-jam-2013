const
CTRL_LEFT = 0;
CTRL_RIGHT = 1;
CTRL_UP = 2;
CTRL_DOWN = 3;
CTRL_JUMP = 4;
CTRL_RUN = 5;
CTRL_START = 6;
CTRL_BACK = 7;
end

global
keyb_map[7] = _a,_d,_w,_s,_k,_j, _enter, _esc;
end

function control(button)
begin
    if (key(keyb_map[button]))
        return true;
    end
    
    return false;
end 
