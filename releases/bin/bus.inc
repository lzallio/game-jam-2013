global
	bus_fpg;
end

process bus_process()
public
	char_speed = 4;
end

private
	orig_y;
	float inc_y=1;
	byte anim_running[1] = 1,2;
	inc_x = 3;
end

begin
    ctype = c_scroll;
	x = round((map.layers[0].size.x*map.tileset.tile_size.x)/8);
	y = (map.layers[0].size.y*map.tileset.tile_size.y)-90;
    file = bus_fpg;
	flags = 1;
	graph = anim_running[0];
    loop
        if (inc_x != 0)
		    if(graph == anim_running[0])
			    graph = anim_running[1];
		    else
			    graph = anim_running[0];
		    end
		end
        x+=inc_x;
        
        if (x>(map.layers[0].size.x*map.tileset.tile_size.x)-(10 * map.tileset.tile_size.x))
	        if (character.status == "won")
	            if(inc_x>0)
	            inc_x--;
	            end
	        else
	            character.status = "died";
	        end
	    end
                
        frame;
    end
end
