process congratulation_screen()
begin
    screen_clear();
    write ( 0 , GAME_RESOLUTION_X/2,GAME_RESOLUTION_Y /2,4 , "Congratulations!! You finished the game.");
    loop
        if (control(CTRL_START))
            break;
        end
        frame;
    end
    onexit:
    Delete_text(ALL_TEXT);
end

process title_screen()
begin
    set_mode(480,320,GAME_COLOR_DEPTH);
    screen_clear();
    put_screen(items_fpg,3);
    while (control(CTRL_BACK)) frame; end 
    loop
        if (control(CTRL_START))
            break;
        end
        if (control(CTRL_BACK))
            exit();
        end
        frame;
    end
    onexit:
    Delete_text(ALL_TEXT);
    set_mode(GAME_RESOLUTION_X,GAME_RESOLUTION_Y,GAME_COLOR_DEPTH);
end

process tiredness_meter()
begin
    graph = map_new(100,10,16);
    // Clear the map red
    map_clear(0,graph,rgb(255,0,0));
    x = 5;
    y = 15;
    set_center(0,graph,0,0);
    z = -200;
    loop
        size_x = character.tiredness;
        frame;
    end
    onexit:
    map_unload(0,graph);
end

process fat_man_heart()
private 
	normalgraph,
	graphdead;
end
begin	
	graph = normalgraph = png_load('fat-heart.png');		
	graphdead = png_load('fat-heart-explode.png');
	
	
	x = 15;
    y = 15;
	while(exists(character))
			
		if(character.tiredness > 100)
			graph = graphdead;
		else	
		    size = 100 + character.tiredness;
		end
		
				
		frame;
				
				
    end
    onexit:
    map_unload(0,normalgraph);
    map_unload(0,graphdead);
    
end
