global
	gravity = 1;
	jump_height = -11;
	max_vertical_speed = 12;
	char_speed = 0;
	char_max_speed = 8;
	character_fpg;
End;

Process character_process()
public 
	string status = "standing";
	byte anim_pos = 0;
	byte tiredness = 0;
private
	byte anim_standing[15] = 1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4;
	byte anim_walking[23] = 5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10;
	byte anim_trotting[23] = 11,11,11,12,12,12,13,13,13,14,14,14,15,15,15,16,16,16,17,17,17,18,18,18;
	byte anim_running[15] = 19,19,19,20,20,20,21,21,22,22,22,23,23,23,24,24;
	byte anim_dying[35] = 35,35,36,36,37,37,38,38,39,39,40,40,41,41,41,42,42,42,43,43,43,43,43,44,44,44,45,45,45,46,46,46,47,47,47,48;
	byte anim_jumping[19] = 24,24,25,25,26,26,27,27,28,28,29,29,30,30,31,31,33,33,34,34;
	
	
	byte pointer anim_pointer;
	byte count;
	float inc_x = 0; //velocidad horizontal
	inc_y = 0; //velocidad vertical
	int y_prev = 0;
	int collision_bottom = 0; //colision abajo
	int collision_top = 0; //colision arriba
	int collision_left = 0; //colision izquierda
	int collision_right = 0; //colision derecha
	
	int pressed_jump = 0; //puede disparar
	
	i = 0;
	j = 0;
	int char_height = 16;
	int char_width = 16;
	current_song = -1;
	playing_wav;
Begin
    ctype = c_scroll;
    
	file = character_fpg;
	x = 32;
	y = 32;
	cnumber = C_0 + C_1;
	// ponemos sonido de ambientacion :P
    playing_wav=play_wav(latidos,-1);  
	loop
		
		// Gravedad
		inc_y += gravity;
		if(inc_y > max_vertical_speed)
			inc_y = max_vertical_speed;
		end
		y_prev = y;
		y += inc_y;
		x += inc_x;

		// Chequeo de colisiones

		// BOTTOM
		for(i = 0; i <= char_height; i++)
			for(j = -(char_height/2); j <= (char_height/2); j++)
				collision_bottom=tilemap_get_pixel(map,0,x+j,y+i);
				if(collision_bottom == RGB(255,0,0) or (collision_bottom == RGB(0,255,0) and (y+i-char_height>y_prev) and inc_y >= 0) and !(control(CTRL_DOWN) and control(CTRL_JUMP)))
					y--;
					inc_y = 0;
					if((status == "jumping" and anim_pos > 3))
						status = "standing";
					end
				end
			end
		end
		// TOP
		for(i = 0; i <= char_height; i++)
			for(j = -(char_height/2); j <= (char_height/2); j++)
				collision_top=tilemap_get_pixel(map,0,x+j,y-i);
				if(collision_top == RGB(255,0,0))
					y++;
					if(inc_y < 0)
						inc_y *= -1;
					end
				end
			end
		end
		// LEFT
		for(i = 0; i <= char_width; i++)
			for(j = -(char_width/2); j <= (char_width/2); j++)
				collision_left=tilemap_get_pixel(map,0,x-i,y+j);
				if(collision_left == RGB(255,0,0))
					x++;
					if(inc_x < 0)
						inc_x = 0;
					end
				end
			end
		end
		// RIGHT
		for(i = 0; i <= char_width; i++)
			for(j = -(char_width/2); j <= (char_width/2); j++)
				collision_right=tilemap_get_pixel(map,0,x+i,y+j);
				if(collision_right == RGB(255,0,0))
					x--;
					if(inc_x > 0)
						inc_x = 0;
					end
				end
			end
		end
		// Fin de chequeo de colisiones

		if(!control(CTRL_JUMP) and (status == "standing" or status == "running") and inc_y == 0)
			pressed_jump = 0;
			inc_y++;
		elseif(control(CTRL_JUMP) and inc_y > 5)
			pressed_jump = 1;
		elseif(!control(CTRL_JUMP) and (status == "standing" or status == "running") and inc_y > 3)
			status = "jumping";
			anim_pos = 18;
			anim_pointer = &anim_jumping;
			count = sizeof(anim_jumping)/sizeof(byte);
		end
		
		
		if(!control(CTRL_JUMP) and status == "jumping")
			inc_y++;
		end
		
		
		// Motor de estados
		switch(status):
			case "running":
			    
			    
			    
				if(control(CTRL_JUMP) and status != "jumping" and pressed_jump == 0 and inc_y <= 0)
					status = "jumping";
					anim_pos = 0;
					anim_pointer = &anim_jumping;
					count = sizeof(anim_jumping)/sizeof(byte);
					pressed_jump = 1;
				elseif(!control(CTRL_RIGHT) and !control(CTRL_LEFT))
					status = "standing";
					anim_pos = 21;
					anim_pointer = &anim_standing;
					count = sizeof(anim_standing)/sizeof(byte);
				else
				    if (control(CTRL_RUN))
					    char_speed = char_max_speed;
					else
					    char_speed = char_max_speed/3;
					end
					
					if((collision_left == RGB(0,0,255) and inc_x < 0) or (collision_right == RGB(0,0,255) and inc_x > 0))
						char_speed -= 2;
						inc_y -= 1;
					end
					if(control(CTRL_RIGHT))
					
						if(inc_x < char_speed)
							inc_x += 0.5;
						elseif(inc_x > char_speed)
							inc_x -= 0.5;
						end
						flags = 0;
					elseif(control(CTRL_LEFT))
						flags = 1;
						if(inc_x > (char_speed*(-1)))
							inc_x-= 0.5;
						elseif(inc_x < (char_speed*(-1)))
							inc_x+= 0.5;
						end
					end
					
					
				end
			end
			case "standing":
			    
			    
			    
				if(control(CTRL_JUMP) and status != "jumping" and pressed_jump == 0 and inc_y <= 0)
					status = "jumping";
					anim_pos = 0;
					anim_pointer = &anim_jumping;
					count = sizeof(anim_jumping)/sizeof(byte);
					pressed_jump = 1;
					if(inc_x > 0)
						inc_x-= 0.5;
					elseif(inc_x < 0)
						inc_x+= 0.5;
					end
				elseif(control(CTRL_RIGHT) or control(CTRL_LEFT))
					status = "running";
					anim_pos = 0;
					
					
				elseif(control(CTRL_DOWN) and control(CTRL_JUMP))
					if(collision_bottom == RGB(0,255,0))
						anim_pointer = &anim_jumping;
					end
				else
					if(inc_x > 0) //frenando
						inc_x-= 0.5;
						
					elseif(inc_x < 0) //frenando
						inc_x+= 0.5;
						
					else // frenado
						anim_pointer = &anim_standing;
						count = sizeof(anim_standing)/sizeof(byte);
					end
					if(collision_bottom == RGB(0,0,255))
						inc_y -= 1;
					end
				end
			end
			case "died":
                
			    if (anim_pointer != &anim_dying)
			        anim_pos = 0;
				    anim_pointer = &anim_dying;
			        count = sizeof(anim_dying)/sizeof(byte);
				else
				    if (anim_pos >= count-1)
		    
	                    anim_pos = count -2;
		            end
				end
			    //frenando
			    if(inc_x > 0)
					inc_x-= 0.5;
				elseif(inc_x < 0) 
					inc_x+= 0.5;
				end 
			end
			case "jumping":
			    
				if(control(CTRL_RIGHT) or control(CTRL_LEFT))
					if (control(CTRL_RUN))
					    char_speed = char_max_speed;
					else
					    char_speed = char_max_speed/3;
					end
					if(control(CTRL_RIGHT))
						if(inc_x < char_speed)
							inc_x+= 0.5;
						end
						flags = 0;
					elseif(control(CTRL_LEFT))
						flags = 1;
						if(inc_x > (char_speed*(-1)))
							inc_x-= 0.5;
						end
					end
				end
				
				if(collision_bottom == RGB(255,0,0))
					inc_y = 0;
					anim_pointer = &anim_standing;
					count = sizeof(anim_standing)/sizeof(byte);
					status = "standing";
				end;
				if (anim_pos == 1)
					inc_y = jump_height;
					
				end
				if (anim_pos >= 18)
					anim_pos = 18;
				end
			end
		    case "won":
			    if(inc_x > 0) //frenando
				    inc_x-= 0.5;
				
			    elseif(inc_x < 0) //frenando
				    inc_x+= 0.5;
				
			    else // frenado
				    anim_pointer = &anim_standing;
				    count = sizeof(anim_standing)/sizeof(byte);
			    end
			    if(collision_bottom == RGB(0,0,255))
				    inc_y -= 1;
			    end
			end
			
		end
		
		// mientras corre se cansa
	    
	    if (tiredness<100)
			if (control(CTRL_RUN))
				tiredness += abs(inc_x)/(char_max_speed/2);
			elseif(tiredness>0)
			    tiredness --;
			end
	    else
	        status = "died";
	    end
	    
	    if (tiredness>100)
	    tiredness = 100;
	    end
	    
	    // volumen de los latidos del corazon
	    set_distance ( playing_wav , 255 - ((255 * tiredness)/100) ); 
	    set_song_volume (128 - ((128 * tiredness)/100));   
	    
	    
	    // si se cae pierde
	    if (y>map.layers[0].size.y*map.tileset.tile_size.y)
	        status = "died";
	    end
	    
	    // limites de la pantalla.
	    if (x<char_width)
	        x = char_width;
	    end
	    if (x>(map.layers[0].size.x*map.tileset.tile_size.x)-char_width)
	        x=(map.layers[0].size.x*map.tileset.tile_size.x)-char_width;
	    end
	    
	    // si llega a la meta gana (10 tiles al final del stage)
	    if (x>(map.layers[0].size.x*map.tileset.tile_size.x)-(10 * map.tileset.tile_size.x))
	        status = "won";
	        
	    end
	    
		// animaciones para cuando corre dependiendo de la velocidad
		if((status == "running" or status=="standing" or status == "won") and ((int)inc_x)!=0)
		    switch(abs(inc_x))
		        case 0..2:
		        anim_pointer = &anim_walking;
			    count = sizeof(anim_walking)/sizeof(byte);
		        end
		        case 3..5:
		        anim_pointer = &anim_trotting;
			    count = sizeof(anim_trotting)/sizeof(byte);
			    end
			    case 6..8:
			    anim_pointer = &anim_running;
			    count = sizeof(anim_running)/sizeof(byte);
		        end
		    end
			
		end
		

		// Cambia el frame de animación 
		anim_pos++;
		if (anim_pos >= count)
			anim_pos = 0;
		end
		
		// Muestra el grafico correspondiente
		
		graph = anim_pointer[anim_pos];
		
		frame;
	end
	onexit:
	if(is_playing_wav(playing_wav))
	     stop_wav(playing_wav);
	end
End
