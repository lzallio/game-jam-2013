// IMPORTS
/*
import "mod_time"


import "mod_scroll";

import "mod_rand";

import "mod_timers";
import "mod_sort";
import "mod_text";
import "mod_file";


import "mod_video"
import "mod_sound"
import "mod_mouse"


*/
import "mod_grproc";
import "mod_wm"
import "mod_key"
import "mod_proc"
import "mod_video"
import "mod_text"
import "mod_map"
import "mod_draw"
import "mod_debug"
import "mod_say";
import "mod_screen";
import "mod_math";
import "mod_time";
import "mod_dir";
import "mod_joy";
import "mod_sound";

include "libraries/tiledmap/tiledmap.lib";

// EDITOR PROGRAM

// editor settings
const
	GAME_RESOLUTION_X = 320;
	GAME_RESOLUTION_Y = 240;

	GAME_COLOR_DEPTH = 16;
end

global
	_tiled_map map; // the map we'll load
	items_fpg;
	latidos;
	music, endmusic;
end

include "controls.inc";
include "char.inc";
global
character_process character;

end
include "items.inc";
include "bus.inc";

global

bus_process bus;
end

include "gui.inc";

process main()
private
	desktop_width = 640;
    desktop_height = 480;
    

    _tiledmap_viewport viewport;
    
    int i,j;
    
    
	current_screen;
	
	return_to_menu = false;
	
	byte stages[5]=1,2,3,4,5,6; // lista de pantallas
	byte current_stage;
    
begin
	
    // adjust the size of the game to the native resolution
        get_desktop_size(&desktop_width, &desktop_height);
        
        scale_resolution = desktop_width * 10000 + desktop_height ;


        

        scale_resolution_aspectratio = SRA_PRESERVE;
        // put the game in full screen
        //scale_mode = SCALE_HQ2X;
        full_screen = true;


	set_fps(30,0);
    set_mode(GAME_RESOLUTION_X,GAME_RESOLUTION_Y,GAME_COLOR_DEPTH);
	
    // carga recursos
    character_fpg = load_fpg('character/character.fpg');
    items_fpg = load_fpg('character/items.fpg');
    bus_fpg = load_fpg('character/bus.fpg');
    
    // carga sonidos
    latidos = load_wav("sounds/latido_5.wav");
    music = load_song("sounds/CardiacRunnerLoop1Master.ogg");
    endmusic = load_song("sounds/CardiacRunnerEndMaster.ogg");
    
    
    
    loop
        return_to_menu = false; // reset flag status
          
        // pantalla de titulos
	    current_screen = title_screen();
	    while (exists(current_screen)) frame; end
	    current_stage = 0; // restart stage counter to first stage
        
        while( current_stage < sizeof(stages))
            return_to_menu = false; // reset flag status
	        // renderiza el mapa cargado
	        tilemap_load_map(map,"stages/" + (current_stage+1) + ".tmx");
	        tilemap_start(viewport, map, 320, 256);
            
            
            // crea objetos en pantalla
            for (i=0; i< map.objectgroup_count ; i++)
                for (j=0; j< map.objectgroups[i].objectcount ; j++)
                    switch(map.objectgroups[i].objects[j].typename)
                        case "banana":
                            banana(map.objectgroups[i].objects[j].x,map.objectgroups[i].objects[j].y);
                        end                        
						case "hamburger":
                            hamburger(map.objectgroups[i].objects[j].x,map.objectgroups[i].objects[j].y);
                        end
                    end
                end
            end
            
            character = character_process();
            bus = bus_process();
            fat_man_heart();
            
            play_song(music,-1);
            
            // loop del juego
            loop
			    
                viewport.x = character.x - (GAME_RESOLUTION_X/2);
                viewport.y = character.y-160;


                if (exit_status or control(CTRL_BACK))
                    return_to_menu = true;
                    break;
                end
                
                
                if (character.status == "died")
                    write ( 0 , GAME_RESOLUTION_X/2,GAME_RESOLUTION_Y /2,4 , "CARDIAC ARREST! Press ENTER to try again");
                    fade_music_off(1000);
                    while (!control(CTRL_START))
                        frame;
                    end
                    break;
                end
                
                if (character.status == "won")
                    current_stage++;
                    write ( 0 , GAME_RESOLUTION_X/2,GAME_RESOLUTION_Y /2,4 , "GREAT YOU DID IT! Press ENTER to catch next");
                    stop_song();
                    play_song(endmusic,0);
                    while (!control(CTRL_START))
                        viewport.x = character.x - (GAME_RESOLUTION_X/2);
                        viewport.y = character.y-160;

                        frame;
                    end
                    
                    
                    
                    break;
                end
                
                frame;
            end
            if (is_playing_song())
            stop_song();
            end
            
            Delete_text(ALL_TEXT);
            tiledmap_stop(viewport);
            tilemap_unload_map(map);
            let_me_alone();
            frame;
            
            if (return_to_menu)
                break;
            end
            
            
                   
        end
        if (!return_to_menu)
        // espera que se suelte el boton
            while (control(CTRL_START)) frame; end
            // pantalla de felicitaciones al terminar el juego
            current_screen = congratulation_screen();
            while (exists(current_screen)) frame; end
        end
        
        
	end

end
